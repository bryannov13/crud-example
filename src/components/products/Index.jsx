import React, {useState, useEffect} from 'react';
import Form from './Form'
import List from './List'
import db from '../../utils/firebase';

export default function Index() {
    const table = "Products";
    const [itemList,setItemList] = useState([]);
    const [currentId,setCurrentId] = useState("");

    const addOrEdit = async (item)=>{
        if (currentId==="") {
            await db.collection(table).doc().set(item);
        }
        else{
            await db.collection(table).doc(currentId).update(item);
        }
    }

    const onDelete = async (id)=>{
        if(window.confirm("Seguro que desea eliminar?")){
            await db.collection(table).doc(id).delete();
        }
    }

    const getList = async ()=>{
        await db.collection(table).orderBy('title').onSnapshot(query => {
            const docs = [];
            query.forEach(
                    (doc) => {docs.push({...doc.data(),id:doc.id})}
                )
            setItemList(docs);
        });
    }

    useEffect(() => {
        getList();
    }, [])
    
    return (
        <>
            <Form table={table} addOrEdit = {addOrEdit} currentId={currentId} setCurrentId={setCurrentId}/>
            <List itemList={itemList} onDelete={onDelete} setCurrentId={setCurrentId}/>
        </>
    );
}