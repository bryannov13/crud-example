import React, {useState, useEffect} from 'react';
import db from '../../utils/firebase';


export default function Form(props) {
    const defaultValues = {
                            title:"",
                            stock:"",
                            price:"",
                            description:""
                        }
    const [values,setValues] = useState(defaultValues);

    const handleSubmit= e =>{
        e.preventDefault();
        props.addOrEdit(values);
        cleanForm();
    }

    const handleChange = e =>{
        const {name,value} = e.target
        setValues({...values,[name]:value});
    }

    const setCurrentItem = async(id)=>{
        setValues({...((await db.collection(props.table).doc(id).get()).data())});
    }

    const cleanForm = ()=>{
        props.setCurrentId("");
        setValues(defaultValues);
    }

    useEffect(() => {
        if(props.currentId===""){
            setValues({...defaultValues})
        }
        else{
            setCurrentItem(props.currentId)
        }
    },[props.currentId])

    return (
        <form 
            onSubmit={handleSubmit}
            className="card card-body col-md-4 offset-1 mt-2">
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">assessment</i>
                </div>
                <input 
                    type="text"
                    className="form-control"
                    placeholder="Titulo"
                    name="title"
                    value={values.title}
                    onChange={handleChange}
                    />
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">business_center</i>
                </div>
                <input 
                    type="number"
                    className="form-control"
                    placeholder="Stock"
                    name="stock"
                    value={values.stock}
                    onChange={handleChange}
                    />
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">attach_money</i>
                </div>
                <input 
                    type="number"
                    className="form-control"
                    placeholder="Precio"
                    name="price"
                    value={values.price}
                    onChange={handleChange}
                    />
            </div>
            <div className="form-group input-group">
                <textarea 
                    rows="5" 
                    cols=""
                    className="form-control"
                    placeholder="Descripcion"
                    name="description"
                    value={values.description}
                    onChange={handleChange}
                    ></textarea>
            </div>
            <div className="d-flex justify-content-between">
                <button type="submit" className="col-6 btn btn-primary">
                    {props.currentId===""?"Agregar":"Actualizar"}
                </button>
                <button type="reset" className="col-4 btn btn-secondary" onClick={()=>cleanForm()}>limpiar</button>
                
            </div>
        </form>
    );
}