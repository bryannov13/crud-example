import React from 'react';


export default function List(props) {
    
    function ItemList(props) {
        return(
            <div className="list-group-item  d-flex justify-content-between">
                <a href={"/"+props.item.id} className=" col-8 list-group-item-action">{props.item.title}</a>
                <button className="btn btn-link" onClick={()=>props.setCurrentId(props.item.id)}>
                    <i className="material-icons text-light">edit</i>
                </button>
                <button className="btn btn-link" onClick={()=>props.onDelete(props.item.id)}>
                    <i className="material-icons text-danger">delete</i>
                </button>
            </div>
        )
    }
    
    const items = props.itemList.map(
                        (item) => <ItemList key={item.id} item = {item} onDelete={props.onDelete} setCurrentId={props.setCurrentId}/>
                    )
    return(
            <section className="col-md-4 offset-1 mt-2">
                <div className="list-group">
                    {items}
                </div>
            </section>
    )
}